package br.com.projetos.advice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import br.com.projetos.controller.Controller;
import br.com.projetos.exceptions.LoginExistenteException;
import br.com.projetos.protocol.Protocol;

@RestControllerAdvice(assignableTypes = Controller.class)
public class LoginAdvice {

	private Logger log = LoggerFactory.getLogger(LoginAdvice.class);

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Protocol> loginInvalido(MethodArgumentNotValidException e) {

		log.error("MethodArgumentNotValidException {}", e.getMessage());

		return ResponseEntity.badRequest().body(Protocol.INVALIDO);

	}

	@ExceptionHandler(LoginExistenteException.class)
	public ResponseEntity<Protocol> loginExistente(LoginExistenteException e) {

		log.error("LoginExistenteException {}", e.getMessage());

		return ResponseEntity.status(HttpStatus.CONFLICT).body(Protocol.EXISTENTE);

	}

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<String> loginEndPointNaoEncontrado(MethodArgumentTypeMismatchException e) {

		log.error("MethodArgumentTypeMismatchException {}", e.getMessage());

		return ResponseEntity.badRequest().body("LOGIN TIPO INVALIDO");

	}

}
