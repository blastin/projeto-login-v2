package br.com.projetos.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.util.DigestUtils;

import lombok.Value;

@Value
public class LoginDTO {

	private static final String REGEX = "^(?!.* )(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%&\\*]).{8,}$";

	@Size(min = 4, max = 12)
	String name;

	@Pattern(regexp = REGEX)
	String password;

	@Email
	String email;

	public String getPassword() {
		return DigestUtils.md5DigestAsHex(password.getBytes());
	}
}
