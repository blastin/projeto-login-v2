package br.com.projetos.exceptions;

import br.com.projetos.proxy.LoginProxy;
import lombok.NonNull;

public class LoginExistenteException extends Exception {

	private final LoginProxy proxy;

	public LoginExistenteException(@NonNull final LoginProxy proxy) {
		this.proxy = proxy;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 5945311953367712532L;

	@Override
	public String getMessage() {
		return "Cadastro de login com email[" + proxy.getEmail() + "] já existe em base de dados";
	}

}
