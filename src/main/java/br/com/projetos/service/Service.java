package br.com.projetos.service;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.projetos.dto.LoginDTO;
import br.com.projetos.exceptions.LoginExistenteException;
import br.com.projetos.factory.LoginFactory;
import br.com.projetos.model.Login;
import br.com.projetos.protocol.Protocol;
import br.com.projetos.proxy.LoginProxy;
import br.com.projetos.repository.Repository;

@org.springframework.stereotype.Service
public class Service {

	private Logger log = LoggerFactory.getLogger(Service.class);

	@Autowired
	private Repository repository;

	@Autowired
	private LoginFactory factory;

	public Protocol salvar(LoginDTO dto) throws LoginExistenteException {

		try {

			log.info("Service::salvar[LoginDTO] : {}", dto);

			LoginProxy findByEmailOrName = repository.findByEmailOrName(dto.getEmail(), dto.getName());

			throw new LoginExistenteException(findByEmailOrName);

		} catch (NullPointerException e) {

			log.info("Service::salvar[LoginDTO] saving ... ");

			Login login = factory.dtoToModel(dto);

			repository.save(login);

			return Protocol.SALVO;

		}

	}

	public Collection<LoginProxy> obterTodosLogins() {

		log.info("Service::obterTodosLogins ... ");

		return repository.findByNameIsNotNull();

	}

}