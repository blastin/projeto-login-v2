package br.com.projetos.advice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import br.com.projetos.controller.ControllerStatus;

@RestControllerAdvice(assignableTypes = ControllerStatus.class)
public class StatusAdvice {

	private Logger log = LoggerFactory.getLogger(StatusAdvice.class);

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public ResponseEntity<String> statusInvalido(MethodArgumentTypeMismatchException e) {

		log.error("MethodArgumentTypeMismatchException {}", e.getMessage());

		return ResponseEntity.badRequest().body("STATUS TIPO INVALIDO");

	}
}
