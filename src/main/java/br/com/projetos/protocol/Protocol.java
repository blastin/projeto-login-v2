package br.com.projetos.protocol;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Protocol
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@JsonFormat(shape = Shape.OBJECT)
public enum Protocol {

	SALVO(1000, "Login de usuário foi cadastrado com sucesso"),
	EXISTENTE(1001, "Login de usuário já existe em base de dados"),
	INVALIDO(1002,"Login incompleto ou invalido");
	
	private final int status;

	private final String descricao;

}