package br.com.projetos.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.projetos.model.Login;
import br.com.projetos.proxy.LoginProxy;

/**
 * Repository
 */
public interface Repository extends JpaRepository<Login, Long> {

	LoginProxy findByEmailOrName(String email, String name);

	Collection<LoginProxy> findByNameIsNotNull();

}