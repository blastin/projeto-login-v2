package br.com.projetos.controller;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.projetos.dto.LoginDTO;
import br.com.projetos.exceptions.LoginExistenteException;
import br.com.projetos.protocol.Protocol;
import br.com.projetos.proxy.LoginProxy;
import br.com.projetos.service.Service;

@RestController
@RequestMapping("api/login/")
public class Controller {

	private Logger log = LoggerFactory.getLogger(Controller.class);

	@Autowired
	private Service service;

	@PostMapping("save")
	public ResponseEntity<Protocol> salvarLogin(@RequestBody @Validated LoginDTO dto)
			throws LoginExistenteException {

		Protocol salvar = service.salvar(dto);

		log.info("salvarLogin[Protocol] : {}", salvar.getDescricao());

		return ResponseEntity.status(HttpStatus.CREATED).body(salvar);

	}

	@GetMapping("todos")
	public ResponseEntity<Collection<LoginProxy>> obterTodosLogins() {

		log.info("obterTodosLogins");

		return ResponseEntity.ok(service.obterTodosLogins());

	}
}
