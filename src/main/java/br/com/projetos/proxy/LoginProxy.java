package br.com.projetos.proxy;

public interface LoginProxy {

	String getName();

	String getEmail();

}
