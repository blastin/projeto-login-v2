package br.com.projetos.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import br.com.projetos.dto.LoginDTO;
import br.com.projetos.model.Login;

@Component
public class MapperLogin {

	private ModelMapper mapper;

	public MapperLogin(ModelMapper mapper) {
		this.mapper = mapper;
	}

	public Login by(LoginDTO dto) {
		return mapper.map(dto, Login.class);
	}

}
