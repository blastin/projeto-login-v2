package br.com.projetos.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/status/")
public class ControllerStatus {

	@GetMapping("ok/{loopback}")
	public ResponseEntity<Integer> status(@PathVariable @Validated Integer loopback) {
		return ResponseEntity.ok(loopback);
	}
}
