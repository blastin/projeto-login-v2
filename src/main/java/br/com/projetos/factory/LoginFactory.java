package br.com.projetos.factory;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.projetos.dto.LoginDTO;
import br.com.projetos.mapper.MapperLogin;
import br.com.projetos.model.Login;

@Component
public final class LoginFactory {

    @Autowired
    private MapperLogin mapper;

    public Login dtoToModel(LoginDTO dto) {

        Login login = mapper.by(dto);

        LocalDateTime now = LocalDateTime.now();

        login.setDataCriacao(now);
        login.setDataModificacao(now);

        return login;

    }
}